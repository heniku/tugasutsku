import 'package:flutter/material.dart';
import './control.dart';

class Output extends StatefulWidget{
  @override
  _OutputState createState() => _OutputState();
}

class _OutputState extends State<Output>{
  String msg = 'Unisnu';

  void _changeText() {
    setState(() {
      if (msg.startsWith('K')) {
        msg = 'Cendekia dan';
      } else if (msg.startsWith('A')) {
        msg = 'Cendekia dan';
      }else{
        msg = 'Ahlakul Karimah';
      }
      });
    }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('UNISNU ', style: new TextStyle(fontSize:30.0),),
          Control(msg),
          RaisedButton(child: Text("CLICK",style: new TextStyle( color: Colors.black),),color: Colors.blueGrey,onPressed:_changeText,),
        ],
      ),
    );
  }
}