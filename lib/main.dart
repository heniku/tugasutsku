import 'package:flutter/material.dart';
import './output.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Tugas UTS Heni',
      home: Scaffold(
        backgroundColor: Colors.blue[100],
        appBar: AppBar(
          backgroundColor: Colors.blue,
          leading: new Icon(Icons.home),
          title: Text('Tugas UTS Heni'),
          actions: <Widget>[
          new Icon(Icons.search),
        ],
        ),
        body: Output(),
      ),
    );
  }
}